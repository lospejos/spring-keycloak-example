package com.resource;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class ResourceServer {

  public static void main(final String[] args) {
    SpringApplication.run(ResourceServer.class, args);
  }

}
